<?php
include "header.php";

$fileName = $postedData["fileName"];
$userId = $postedData["userId"];

$file1 = '../media-manager/'.$userId.'/submit/'.$fileName;
$file2 = '../media-manager/'.$userId.'/saved/'.$fileName;

if (!unlink($file1) && !unlink($file2)) {  
    $data = 'fail';
}  
else {  
    $data = 'success';
}  

echo json_encode($data);
?>