<?php 
// required headers for post
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$userId = $_POST['userId'];
$fileName = $_POST['fileName'];
$saved = $_POST['saved'];
$imagesToBeUploaded = $_POST['imagesToBeUploaded'];

$ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
//$fileName = $fileName.".".$ext;
mkdir("../media-manager/".$userId, 0777, true);
mkdir("../media-manager/".$userId."/submit", 0777, true);
mkdir("../media-manager/".$userId."/saved", 0777, true);


$count = count($_FILES['image']);

if($saved === 'true'){
	for ($i = 0; $i < $count; $i++) {
		move_uploaded_file($_FILES["image"]["tmp_name"][$i], "../media-manager/".$userId."/saved/".$fileName[$i]);
		//move_uploaded_file($_FILES["image"]["tmp_name"][$i], "../media-manager/".$userId."/submit/".$fileName[$i]);
		copy("../media-manager/".$userId."/saved/".$fileName[$i], "../media-manager/".$userId."/submit/".$fileName[$i]);
}
} else {
	for ($i = 0; $i < $count; $i++) {
		move_uploaded_file($_FILES["image"]["tmp_name"][$i], "../media-manager/".$userId."/submit/".$fileName[$i]);
	}
}
$data = "hello";
echo json_encode($userId);
?>