<?php
require "./db.php";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>ONITT > Reset Password</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
   
.login-register {
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
    height: 100%;
    width: 100%;
    padding: 5% 0;
    position: fixed;
	display:flex;
	align-items:center;
}
.login-box {
    width: 400px;
    margin: 0 auto;
}.card-body.inbox-panel {
    height: 100vh;
	    padding-top: 8px;
    overflow: auto;
}
.btn-info, .btn-info.disabled {
    background: #1976d2;
    border: 1px solid #1976d2;
    -webkit-box-shadow: 0 2px 2px 0 rgba(66,  165,  245,  0.14),  0 3px 1px -2px rgba(66,  165,  245,  0.2),  0 1px 5px 0 rgba(66,  165,  245,  0.12);
    box-shadow: 0 2px 2px 0 rgba(66,  165,  245,  0.14),  0 3px 1px -2px rgba(66,  165,  245,  0.2),  0 1px 5px 0 rgba(66,  165,  245,  0.12);
    -webkit-transition: 0.2s ease-in;
    -o-transition: 0.2s ease-in;
    transition: 0.2s ease-in;
}
.btn-info.disabled:hover, .btn-info:hover {
    background: #1976d2;
    border: 1px solid #1976d2;
    -webkit-box-shadow: 0 14px 26px -12px rgba(23,  105,  255,  0.42),  0 4px 23px 0 rgba(0,  0,  0,  0.12),  0 8px 10px -5px rgba(23,  105,  255,  0.2);
    box-shadow: 0 14px 26px -12px rgba(23,  105,  255,  0.42),  0 4px 23px 0 rgba(0,  0,  0,  0.12),  0 8px 10px -5px rgba(23,  105,  255,  0.2);
}
.btn-info.active, .btn-info.disabled.active, .btn-info.disabled:active, .btn-info.disabled:focus, .btn-info:active, .btn-info:focus {
    background: #028ee1;
    -webkit-box-shadow: 0 14px 26px -12px rgba(23,  105,  255,  0.42),  0 4px 23px 0 rgba(0,  0,  0,  0.12),  0 8px 10px -5px rgba(23,  105,  255,  0.2);
    box-shadow: 0 14px 26px -12px rgba(23,  105,  255,  0.42),  0 4px 23px 0 rgba(0,  0,  0,  0.12),  0 8px 10px -5px rgba(23,  105,  255,  0.2);
}.form-horizontal label {
    margin-bottom: 0px;
}
.form-material .form-group {
    overflow: hidden;
}
.form-material .form-control {
    background-color: rgba(0,  0,  0,  0);
    background-position: center bottom,  center calc(100% - 1px);
    background-repeat: no-repeat;
    background-size: 0 2px,  100% 1px;
    padding: 0;
    -webkit-transition: background 0s ease-out 0s;
    -o-transition: background 0s ease-out 0s;
    transition: background 0s ease-out 0s;
}
.form-material .form-control, .form-material .form-control.focus, .form-material .form-control:focus {
    background-image: -webkit-gradient(linear,  left top,  left bottom,  from(#1976d2),  to(#1976d2)),  -webkit-gradient(linear,  left top,  left bottom,  from(rgba(120,  130,  140,  0.13)),  to(rgba(120,  130,  140,  0.13)));
    background-image: -webkit-linear-gradient(#1976d2,  #1976d2),  -webkit-linear-gradient(rgba(120,  130,  140,  0.13),  rgba(120,  130,  140,  0.13));
    background-image: -o-linear-gradient(#1976d2,  #1976d2),  -o-linear-gradient(rgba(120,  130,  140,  0.13),  rgba(120,  130,  140,  0.13));
    background-image: linear-gradient(#1976d2,  #1976d2),  linear-gradient(rgba(120,  130,  140,  0.13),  rgba(120,  130,  140,  0.13));
    border: 0 none;
    border-radius: 0;
    -webkit-box-shadow: none;
    box-shadow: none;
    float: none;
}
.form-material .form-control.focus, .form-material .form-control:focus {
    background-size: 100% 2px,  100% 1px;
    outline: 0 none;
    -webkit-transition-duration: 0.3s;
    -o-transition-duration: 0.3s;
    transition-duration: 0.3s;
}
.error{
	color: red;
    font-size: 14px;
    padding: 10px;
	font-weight:bold;
}
.successMsg{
	color: green;
    font-size: 14px;
    padding: 10px;
	font-weight:bold;
}
    </style>
</head>
<body>
    
    <section id="wrapper">
        <div class="login-register" style="background-image:url(./images/login-register.jpg);">        
            <div class="login-box card">
            <div class="card-body">
                
    <div id="wrap">
        <!-- start PHP code -->
        <?php
         
         if(isset($_GET['email']) && !empty($_GET['email']) AND isset($_GET['hash']) && !empty($_GET['hash'])){
            // Verify data
            $email = $_GET['email']; // Set email variable
            $hash = $_GET['hash']; // Set hash variable
            $sqlValidate = "SELECT email, hash FROM login WHERE email='".$email."' AND hash='".$hash."'";
	        $mainResult  = $conn->query($sqlValidate);
	        if ($mainResult->num_rows > 0) {
                echo '<form class="form-horizontal form-material" id="contact" action=""  method="post">
				<h3 class="box-title m-b-20">Reset Password</h3>
				<div class="form-group">
				  <div class="col-xs-12">
					<input type="password" id="password" name="password" placeholder="Enter Password" class="form-control" required>
				  </div>
				</div>
				<div class="form-group">
				  <div class="col-xs-12">
					<input type="password" id="rePassword" name="rePassword" placeholder="Re-enter Password" class="form-control" required>
				  </div>
				</div>
                <div class="form-group text-center m-t-20">
				  <div class="col-xs-12">
					<button type="submit" name="submit" id="form-submit" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" >Change</button>
				  </div>
				</div>
                ';            
                if(isset($_POST['submit'])){
                    $password=$_POST['password'];
                    $rePassword=$_POST['rePassword'];
                    if(!$password || !$rePassword){
                        echo '<div class="error">Please Enter Passwords</div>';
                        return;
                    } 
                    if($password != $rePassword){
                        echo '<div class="error">Passwords do not match</div>';
                        return;
                    }
                    if(preg_match("/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/",$password)){
                    $newHash = md5( rand(0,1000) );
                    $sqlUpdate = "UPDATE login SET hash = '".$newHash."', password = '".$password."' WHERE email='".$email."'";
                    $updateResult  = $conn->query($sqlUpdate);
                    if($updateResult){
                        echo '<div class="successMsg">Password successfully updated</div>';
                        echo "<script> location.href='http://store.onitt.co'; </script>";
                        exit;
                    }
                } else {
                    echo '<div class="error">Password must contain atleast 8 characters, one lowercase letter, one uppercase letter and special character</div>';
                }
                }
                echo "</form>";
            }else{
                // No match -> invalid url or account has already been activated.
                echo '<div class="statusmsg">The url is either invalid or you already have reset the password.</div>';
            }
        }else{
            // Invalid approach
            echo '<div class="statusmsg">Invalid approach, please use the link that has been send to your email.</div>';
        }
        ?>
        <!-- stop PHP Code -->
 
         
    </div>
          </div>
        </div>
        
    </section> 
    
    <!-- end wrap div --> 
</body>
</html>