<?php 
// required headers for post
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
$postedData = json_decode(file_get_contents("php://input"), true);

$userId = $postedData["userId"];

$log_directory = '../media-manager/'.$userId.'/submit';
$results_array = array();

if (is_dir($log_directory)) 
{
        if ($handle = opendir($log_directory))
        {
                //Notice the parentheses I added:
                while(($file = readdir($handle)) !== FALSE)
                {
                        if($file != '.' && $file != '..'){
                        $data['date'] = date("F d Y H:i:s.", filectime('../media-manager/'.$userId.'/submit/'.$file));
                        $data['name'] = $file;
                        $results_array[] = $data;
                        }
                }
                closedir($handle);
        }
}


echo json_encode($results_array);
?>