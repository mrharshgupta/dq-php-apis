<?php
// required headers for post
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require "./db.php";
$userId = 1;

$request_templates        = "SELECT template_id, name, image, route_path, category FROM templates";
$request_templates_result = $conn->query($request_templates);
if ($request_templates_result->num_rows > 0) {
	while ($row = $request_templates_result->fetch_assoc()) {
		http_response_code(200);
		$dataToSend["templateId"] = $row["template_id"];
		$dataToSend["name"]       = $row["name"];
		$dataToSend["image"]      = $row["image"];
		$dataToSend["routePath"]  = $row["route_path"];
		$dataToSend["category"]  = $row["category"];
		$data[]                   = $dataToSend;
	}
	 
} else {
	// query error
	http_response_code(500);
	$data["msg"]   = "No Template Found";
	$data["error"] = true;
	$data["query"] = $request_templates;
}
// $request_applied_template = "SELECT template_id, status, user_template_id FROM user_templates WHERE user_id = '".$userId."' AND status = 'applied'";
// $result = $conn->query($request_applied_template);
// 	 if ($result->num_rows > 0) {
// 	 	$row = $result->fetch_assoc();
// 	 	$data["appliedTemplate"] = $row["template_id"];
// 	 	$data["userTemplateIdTemplate"] = $row["user_template_id"];
// 	 } else {
// 	 	$data["appliedTemplate"] = "No applied Template";
// 	 }

echo json_encode($data);

?>