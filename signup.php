<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require '../composer/vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

include "header.php";
include 'login-function.php';
require_once './domain/vendor/autoload.php';
require_once './domain/addsubdomain.php';

$storeName = $postedData["storeName"];
$email = $postedData["email"];
$password = md5($postedData["password"]);
$user_type = $postedData["user_type"];
$service = $postedData["service"];
$tenantId = $postedData["tenantId"];

$phone = "";
if (isset($postedData["phone"]))
{
    $phone = preg_replace('/(\W*)/', '', $postedData["phone"]);
}

$hash = md5(rand(0, 1000));

$signUp = "INSERT INTO login (store_name, email, password, hash, active, plan, user_type, service, tenant_id) VALUES ('" . $storeName . "','" . $email . "','" . $password . "','" . $hash . "', 1, 1,'". $user_type ."','". $service ."','". $tenantId ."')";

if (isset($storeName) && !empty($storeName))
{
    if (isset($email) && !empty($email))
    {
        if (isset($password) && !empty($password))
        {
            if ($conn->query($signUp))
            {
                http_response_code(200);

                try
                {
                    $mail->SMTPDebug = false;
            $mail->isSMTP();                             
            $mail->Host       = 'smtp.gmail.com';        
            $mail->SMTPAuth   = true;                    
            $mail->Username   = 'info@onitt.co';
            $mail->Password   = 'ajgycyzpqwrxnkqx'; 
            $mail->SMTPSecure = "tls";         
            $mail->Port       = 587;
                    // //Server settings
                    // $mail->SMTPDebug = false; //SMTP::DEBUG_SERVER;
                    // $mail->isSMTP();
                    // $mail->Host = 'smtp.gmail.com';
                    // $mail->SMTPAuth = true;
                    // $mail->Username = 'testingbyharshgupta@gmail.com';
                    // $mail->Password = 'testingbyharshgupta@123';
                    // $mail->SMTPSecure = "tls";
                    // $mail->Port = 587;

                    $mail->setFrom('admin@onitt.co', 'Onitt');
                    $mail->addAddress($email, $storeName);

                    $mail->Subject = 'Signup | Verification';
                    $mail->Body = '<!DOCTYPE html
		PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html>
	
	<head>
		<meta charset="utf-8" content="text/html;" http-equiv="Content-Type" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>Onitt</title>
	
		<style type="text/css">
			.mailWrapp * {
				margin: 0px;
				padding: 0px;
			}
	
			html,
			body {
				margin: 0 !important;
				padding: 0 !important;
				width: 100% !important;
			}
	
			* {
				-ms-text-size-adjust: 100%;
				-webkit-text-size-adjust: 100%;
			}
	
			.mailWrapp {
				max-width: 600px !important;
				width: 100% !important;
			}
		</style>
	</head>
	
	<body>
		<table class="mailWrapp" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" width="600"
			style=" border-collapse:collapse; border: 1px solid #eee" align="center">
			<tbody>
				<tr>
					<td style="padding: 15px;">
						<a href="#" style="width: 150px; margin: auto; display: block;"><img
								src="http://sterlingweb.in/projects/onitt-email/images/logo.png" width="100%" alt=""></a>
					</td>
				</tr>
				<tr>
					<td
						style="color:#00A2A4;font-family:Helvetica;font-size:24px; font-weight: 700; line-height:150%; padding: 15px;">
						Hi ' . $storeName . ',</td>
				</tr>
				<tr>
					<td style="color:#333;font-family:Helvetica;font-size:16px;line-height:150%; padding: 15px;">Thanks for starting your journey with Ointt. Welcome onboard!
						<br />br />
					You are just one step away from creating your dream website. Please click on the button below to verify your email address and start your journey. 
						<br />
						<br />
						<a href="https://api.onitt.co/onitt/files/apis/verify.php?email=' . $email . '&hash=' . $hash . '" style="background: #00A2A4; padding: 10px; color: #fff; text-decoration: none; border-radius: 5px;">Click here to confirm</a>
					</td>
				</tr>
				<tr>
					<td style="color:#333;font-family:Helvetica;font-size:16px;line-height:150%; padding: 15px;">If you face any problems, please try pasting the URL below in your browser:
					<br />
					https://api.onitt.co/onitt/files/apis/verify.php?email=' . $email . '&hash=' . $hash . '
					</td>

				</tr>
				<tr>
					<td style="color:#333;font-family:Helvetica;font-size:16px;line-height:150%; padding: 15px;">Thanks,
						<br /> Onitt Support</td>
				</tr>
				
				
			</tbody>
		</table>
	</body>
	
	</html>';

                    $mail->IsHTML(true);
                    //$mail->send();
                    
                }
                catch(Exception $e)
                {
                    $data['mail'] = false;
                }

                $sqlValidate = "SELECT id FROM login WHERE store_name = '" . $storeName . "' AND password = '" . $password . "'";
                $mainResult = $conn->query($sqlValidate);
                if ($mainResult->num_rows > 0)
                {
                    $row = $mainResult->fetch_assoc();
                    $data["userId"] = $row["id"];
                    $token = $row["id"] . "-" . $password;
                    $token = base64_encode($token);
                    $data["token"] = $token;
                    $data["signUp"] = true;
                }
                else
                {
                    $data["signUp"] = false;
                }
            }
            else
            {
                $data["msg"] = "Error While SignUp";
                $data["signUp"] = false;
            }
        }
        else
        {
            $data["msg"] = "password is missing";
            $data["signUp"] = false;
        }
    }
    else
    {
        $data["msg"] = "Email is missing";
        $data["signUp"] = false;
    }
}

else
{
    $data["msg"] = "Storename is missing";
    $data["signUp"] = false;
}
echo json_encode($data);
?>
