<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require '../composer/vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);


include 'header.php';

$email = $postedData["email"];
$token = $postedData["token"];
$hash = md5( rand(0,1000) );

$query_check_user = "SELECT * FROM login WHERE email = '".$email."'";
$result_check_user = $conn->query($query_check_user);
if($result_check_user->num_rows > 0){

    $row = $result_check_user->fetch_assoc();
    if($row["active"] == "0"){
        $data['status'] = false;
        $data['message'] = 'Please verify your account first.'; 
        $data['query'] = $query_check_user; 
        echo json_encode($data);
        die;
    }
$query_request_password = "UPDATE login SET hash = '".$hash."' WHERE email = '".$email."'";
$result = $conn->query($query_request_password);
if ($result) {
    
        $data['status'] = true;
        $data['message'] = 'Mail containg reset password successfully sent to '.$email;
    
        try {
        
            $mail->SMTPDebug = false;
            $mail->isSMTP();                             
            $mail->Host       = 'smtp.gmail.com';        
            $mail->SMTPAuth   = true;                    
            $mail->Username   = 'info@onitt.co';
            $mail->Password   = 'ajgycyzpqwrxnkqx'; 
            $mail->SMTPSecure = "tls";         
            $mail->Port       = 587;
        //     $mail->SMTPDebug = false; //SMTP::DEBUG_SERVER;       
		// $mail->isSMTP();                             
		// $mail->Host       = 'smtp.gmail.com';        
		// $mail->SMTPAuth   = true;                    
		// $mail->Username   = 'testingbyharshgupta@gmail.com';     
		// $mail->Password   = 'testingbyharshgupta@123';           
		// $mail->SMTPSecure = "tls";         
		// $mail->Port       = 587;  
            $mail->setFrom('admin@onitt.co', 'Onitt');

            // $mail->From = "info@onitt.co";
            // $mail->FromName = "Full Name";
            $mail->addAddress($email, $storeName);    
            
            $mail->isHTML(true); 
            $mail->Subject = 'Reset Password';
            $mail->Body    = '<!DOCTYPE html
            PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html>
        
        <head>
            <meta charset="utf-8" content="text/html;" http-equiv="Content-Type" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <title>Onitt</title>
        
            <style type="text/css">
                .mailWrapp * {
                    margin: 0px;
                    padding: 0px;
                }
        
                html,
                body {
                    margin: 0 !important;
                    padding: 0 !important;
                    width: 100% !important;
                }
        
                * {
                    -ms-text-size-adjust: 100%;
                    -webkit-text-size-adjust: 100%;
                }
        
                .mailWrapp {
                    max-width: 600px !important;
                    width: 100% !important;
                }
            </style>
        </head>
        
        <body>
            <table class="mailWrapp" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" width="600"
                style=" border-collapse:collapse; border: 1px solid #eee" align="center">
                <tbody>
                    <tr>
                        <td style="padding: 15px;">
                            <a href="#" style="width: 150px; margin: auto; display: block;"><img
                                    src="http://sterlingweb.in/projects/onitt-email/images/logo.png" width="100%" alt=""></a>
                        </td>
                    </tr>
                    <tr>
                        <td
                            style="color:#00A2A4;font-family:Helvetica;font-size:24px; font-weight: 700; line-height:150%; padding: 15px;">
                            Hi,</td>
                    </tr>
                    <tr>
                        <td style="color:#333;font-family:Helvetica;font-size:16px;line-height:150%; padding: 15px;">You have requested a password reset for your user account at onitt. Click below to reset your password:
                            <br />
                            <br />
                            <a href="https://api.onitt.co/onitt/files/apis/user-reset-password.php?email='.$email.'&hash='.$hash.'" style="background: #00A2A4; padding: 10px; color: #fff; text-decoration: none; border-radius: 5px;">Reset Password</a>
                        </td>
                    </tr>
                    <tr>
                        <td style="color:#333;font-family:Helvetica;font-size:16px;line-height:150%; padding: 15px;">If you have
                            problems, please paste the above URL into your web browser.</td>
                    </tr>
                    <tr>
                        <td style="color:#333;font-family:Helvetica;font-size:16px;line-height:150%; padding: 15px;">Thanks,
                            <br /> Onitt Support</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="http://sterlingweb.in/projects/onitt-email/images/social.png" usemap="#callmap"
                                style="float:left; margin:0; border:0; padding:0; display:block; height:auto !important; max-width:600px !important; width:100% !important;"
                                border="0" />
                            <map name="callmap">
                                <area shape="rect" coords="180,0,240,50" alt="Computer" href="#" target="_blank">
                                <area shape="rect" coords="241,0,302,50" alt="Computer" href="#" target="_blank">
                                <area shape="rect" coords="303,0,360,50" alt="Computer" href="#" target="_blank">
                                <area shape="rect" coords="361,0,420,50" alt="Computer" href="#" target="_blank">
                            </map>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top"
                            style="background-color:#fafafa;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:9px">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                style="min-width:100%;border-collapse:collapse">
                                <tbody>
                                    <tr>
                                        <td valign="top"
                                            style="color:#333;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center">
                                            <em>Copyright © 2020 Plum Goodness, c/o Pureplay Skin Sciences (India) Pvt Ltd, All
                                                rights reserved.</em><br>
                                            Hi! You are receiving this mail because you signed on to The Plum List.<br>
                                            <br>
                                            <strong>Our mailing address is:</strong><br>
                                            <div><span>Plum Goodness, c/o Pureplay Skin Sciences (India) Pvt Ltd</span>
                                                <div>
                                                    <div>Hiranandani Estate</div><span>Thane</span> <span>400607</span>
                                                    <div>India</div>
                                                </div>
                                            </div>
                                            <br>
                                            <br><a href="#" style="color:#333;font-weight:normal;text-decoration:underline"
                                                target="_blank">update your preferences</a> | <a href="#"
                                                style="color:#333;font-weight:normal;text-decoration:underline"
                                                target="_blank">unsubscribe from this list</a><br>
                                            <br>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </body>
        
        </html>';
            
            $mail->send();
            $data['mail'] = true;
        } catch (Exception $e) {
            $data['mail'] = false;
            $data['error'] = $e->getMessage();
        }


    } else {
        $data['status'] = false;
        $data['message'] = 'Technical Error'; 
        $data['query'] = $query_request_password; 
    }
}
 else {
    $data['status'] = false;
    $data['message'] = 'User not found with email address '.$email; 
    $data['query'] = $query_request_password; 
}
    
    
    
echo json_encode($data);
?>